@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">
                    <div class="d-flex">
                        <h2>All Questions</h2>
                        <div class="ml-auto">
                            <a href="{{ route('questions.create') }}" class="btn btn-outline-primary">Add Question</a>
                        </div>
                    </div>
            </div>
                <div class="card-body">
                    @foreach($questions as $question)
                        <div class="media">
                            <div class="d-flex flex-column counters">
                                <div class="vote">
                                    <strong>{{ $question->votes }}</strong>
                                        @if(($question->votes==1)||($question->votes==-1))
                                        vote
                                        @else
                                        {{ Str::plural('vote') }}
                                        @endif
                                </div>
                                <div class="status {{ $question->status }}">
                                    <strong>{{ $question->answers }}</strong>
                                    @if(($question->answers==1)||($question->answers==-1))
                                        answer
                                        @else
                                        {{ Str::plural('answer') }}
                                        @endif
                                </div>
                                <div class="view">
                                    <strong>{{ $question->views }}</strong> 
                                    @if(($question->views==1)||($question->views==-1))
                                        view
                                        @else
                                        {{ Str::plural('view') }}
                                        @endif
                                </div>
                            </div>
                            <div class="media-body">
                                <h3 class=""><a href="{{ $question->url }}">{{ $question->title }}</a></h3>
                                <p class="lead"> 
                                Asked By <a href="{{ $question->user->url }}">{{ $question->user->name }}</a>
                                    <small> {{ $question->created_date }}</small>
                                </p>
                                {{ Str::limit($question->body, 20) }}
                            </div>
                        </div>
                        <hr>
                    @endforeach
                    {{ $questions->links() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection