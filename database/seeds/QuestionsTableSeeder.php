<?php

use Carbon\Carbon;
use Faker\Generator as Faker;
use Illuminate\Database\Seeder;

class QuestionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('Questions')->insert([
            [
                'id'=>'1',
                'title'=>'This is a trial',
                // 'slug'=>'this-is-a-trial',
                'body'=>'Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea',
                'views'=>'1',
                'answers'=>'0',
                'votes'=>'-1',
                'best_answer_id'=> '0',
                'user_id'=>'1'
            ],

            [
                'id'=>'2',
                'title'=>'This is a adsfasdf',
                // 'slug'=>'this-is-a-tasfadsf',
                'body'=>'Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea',
                'views'=>'12',
                'answers'=>'4',
                'votes'=>'-5',
                'best_answer_id'=>'0',
                'user_id'=>'1'
            ],

            [
                'id'=>'3',
                'title'=>'This is a trialadfa',
                // 'slug'=>'this-is-a-trial2',
                'body'=>'Lorem Ipsum 2DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea',
                'views'=>'1',
                'answers'=>'2',
                'votes'=>'-1',
                'best_answer_id'=>'1',
                'user_id'=>'1'
            ],

            [
                'id'=>'4',
                'title'=>'This is a trialadf',
                // 'slug'=>'this-is-a-triadfal',
                'body'=>'Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea',
                'views'=>'1',
                'answers'=>'2',
                'votes'=>'-1',
                'best_answer_id'=>'1',
                'user_id'=>'1'
            ],
            
            [
                'id'=>'5',
                'title'=>'Thisdfgsfgs',
                // 'slug'=>'this-is-a-tadsfrial',
                'body'=>'Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea Lorem Ipsum DoreaLorem Ipsum Dorea',
                'views'=>'1',
                'answers'=>'2',
                'votes'=>'-1',
                'best_answer_id'=>'1',
                'user_id'=>'1'
            ]
        ]);
    }
}
